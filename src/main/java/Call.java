import java.time.LocalDateTime;

public class Call {
    private int call_id;
    private LocalDateTime callStartTime, callEndTime;

    public Call(int call_id, LocalDateTime callStartTime) {
        this.call_id = call_id;
        this.callStartTime = callStartTime;
    }

    public int getCall_id() {
        return call_id;
    }

    public void setCall_id(int call_id) {
        this.call_id = call_id;
    }

    public LocalDateTime getCallStartTime() {
        return callStartTime;
    }

    public void setCallStartTime(LocalDateTime callStartTime) {
        this.callStartTime = callStartTime;
    }

    public LocalDateTime getCallEndTime() {
        return callEndTime;
    }

    public void setCallEndTime(LocalDateTime callEndTime) {
        this.callEndTime = callEndTime;
    }

    @Override
    public String toString() {
        return "Call{" +
                "call_id=" + call_id +
                ", callStartTime=" + callStartTime +
                ", callEndTime=" + callEndTime +
                '}';
    }
}
