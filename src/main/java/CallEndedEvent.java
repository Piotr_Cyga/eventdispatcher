import java.util.List;

public class CallEndedEvent implements IEvent {
    private int call_id;

    public CallEndedEvent(int call_id) {
        this.call_id = call_id;
    }

    @Override
    public void run() {
        List list = EventDispatcher.instance.getAllObjectsImplementingInterface(ICallListener.class);
        List<ICallListener> listeners = list;
        for (ICallListener listener : listeners) {
            listener.callEnded(call_id);
        }
    }
}
