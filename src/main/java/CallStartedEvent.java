import java.util.ArrayList;
import java.util.List;

public class CallStartedEvent implements IEvent {
    private int call_id;

    public CallStartedEvent(int call_id) {
        this.call_id = call_id;
    }

    @Override
    public void run() {
        List list = EventDispatcher.instance.getAllObjectsImplementingInterface(ICallListener.class);
        List<ICallListener> listeners = list;
        for (ICallListener listener : listeners) {
            listener.callStarted(call_id);
        }
    }
}
