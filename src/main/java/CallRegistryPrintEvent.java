import java.util.List;

public class CallRegistryPrintEvent implements IEvent {
    @Override
    public void run() {
        List list = EventDispatcher.instance.getAllObjectsImplementingInterface(ICallRegistry.class);
        List<ICallRegistry> listeners = list;
        for (ICallRegistry listener : listeners) {
            listener.printListOfCalls();
        }
    }
}
