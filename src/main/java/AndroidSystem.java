import java.util.ArrayList;
import java.util.List;

public class AndroidSystem implements ICallListener {
    private List<Integer> runningCalls = new ArrayList<>();

    public List<Integer> getRunningCalls() {
        return runningCalls;
    }

    @Override
    public void callStarted(int call_id) {
        System.out.println("starting call No." + call_id);
        runningCalls.add(call_id);
    }

    @Override
    public void callEnded(int call_id) {
    if (runningCalls.contains(call_id)) {
        System.out.println("ending call No. " + call_id);
        runningCalls.remove((Integer)call_id);
    }
    else System.out.println("brak takiego połączenia");
    }

    public AndroidSystem() {
        EventDispatcher.instance.registerObject(this);
    }
}
