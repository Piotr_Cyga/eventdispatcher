import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        new AndroidSystem();
        new PhoneApplication();
        Scanner sc = new Scanner(System.in);
        while (sc.hasNextLine()) {
            String line = sc.nextLine();
            int call_id = 0;
            String command = line.split(" ")[0];
            if (!command.equalsIgnoreCase("list")) {

                call_id = Integer.parseInt(line.split(" ")[1]);
            }
            if (command.equalsIgnoreCase("start")) {
                EventDispatcher.instance.dispatch(new CallStartedEvent(call_id));
            } else if (command.equalsIgnoreCase("stop")) {
                EventDispatcher.instance.dispatch(new CallEndedEvent(call_id));
            }
            else if (command.equalsIgnoreCase("list")) {
                EventDispatcher.instance.dispatch(new CallRegistryPrintEvent());
            }
        }
    }
}
