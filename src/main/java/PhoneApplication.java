import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;

public class PhoneApplication implements ICallListener, ICallRegistry{
    private Map<Integer, Call> listOfCalls= new HashMap <> ();


    @Override
    public void callStarted(int call_id) {
    listOfCalls.put(call_id, new Call(call_id, LocalDateTime.now()));
    }

    @Override
    public void callEnded(int call_id) {
    if (listOfCalls.containsKey(call_id)) {
        listOfCalls.get(call_id).setCallEndTime(LocalDateTime.now());
    }
    }

    public PhoneApplication() {
        EventDispatcher.instance.registerObject(this);
    }

    public void printListOfCalls () {
        for (Call c:listOfCalls.values()
             ) {
            System.out.println(c);
        }
    }
}
